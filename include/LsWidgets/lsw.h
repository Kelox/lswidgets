#ifndef LS_WIDGETS_H
#define LS_WIDGETS_H

#include <LsWidgets/lsw_export.h>

#include <LsWidgets/lsw_led.h>
#include <LsWidgets/lsw_builder.h>
#include <LsWidgets/lsw_graph.h>
#include <LsWidgets/lsw_graph_data.h>
#include <LsWidgets/lsw_graph_axis.h>
#include <LsWidgets/lsw_image.h>


// PROTOTYPES //

/// @brief Ensures that all types are registered with the type system
LS_WIDGETS_EXPORT void
ls_widgets_init();

/// @biref Call this right before g_object_unref(application).
/// (Does nothing as of now)
LS_WIDGETS_EXPORT void
ls_widgets_cleanup();

#endif
