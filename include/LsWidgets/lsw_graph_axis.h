#ifndef LSW_GRAPH_AXIS_H
#define LSW_GRAPH_AXIS_H


// INCLUDES //
#include <glib-object.h>
#include <LsWidgets/lsw_export.h>


// TYPE DECLARATION //
#define LSW_TYPE_GRAPH_AXIS lsw_graph_axis_get_type()
LS_WIDGETS_EXPORT G_DECLARE_FINAL_TYPE(LswGraphAxis, lsw_graph_axis, LSW, GRAPH_AXIS, GObject)


// PROTOTYPES //
LS_WIDGETS_EXPORT LswGraphAxis*
lsw_graph_axis_new();

LS_WIDGETS_EXPORT void
lsw_graph_axis_set_display_min( LswGraphAxis*   self,
                                float           minv    );
LS_WIDGETS_EXPORT float
lsw_graph_axis_get_display_min(LswGraphAxis* self);

LS_WIDGETS_EXPORT void
lsw_graph_axis_set_display_max( LswGraphAxis*   self,
                                float           maxv    );
LS_WIDGETS_EXPORT float
lsw_graph_axis_get_display_max(LswGraphAxis* self);

LS_WIDGETS_EXPORT void
lsw_graph_axis_set_display_min_max( LswGraphAxis*   self,
                                    float           minv,
                                    float           maxv    );
LS_WIDGETS_EXPORT void
lsw_graph_axis_get_display_min_max( LswGraphAxis*   self,
                                    float*          minv,
                                    float*          maxv    );


#endif