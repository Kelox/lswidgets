#ifndef LS_WIDGETS_EXPORT_H
#define LS_WIDGETS_EXPORT_H

#if defined(_MSC_VER)
#define LS_WIDGETS_EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
#define LS_WIDGETS_EXPORT __attribute__((visibility("default")))
#else
#define LS_WIDGETS_EXPORT
#endif

#endif