#ifndef LSW_UTILS_H
#define LSW_UTILS_H


// INCLUDES //
#include <glib.h>


// MACROS //
#define LSW_CLAMP(v, minv, maxv) ( (v) < (minv) ? (minv) : ((v) > (maxv) ? (maxv) : (v)) )

#define LSW_SET_PROP(SELF, NAME, VALUE, DTYPE) { \
    GValue value = G_VALUE_INIT; \
    g_value_set_ ## DTYPE ## (&value, VALUE); \
    g_object_set_property(SELF, NAME, &value); }

// PROTOTYPES //
void lsw_utils_set_dark_mode(gboolean p_is_dark);

gboolean lsw_utils_get_dark_mode();


#endif