#ifndef LS_WIDGETS_LED_H
#define LS_WIDGETS_LED_H


// INCLUDES //
#include <gtk/gtk.h>
#include <LsWidgets/lsw.h>


// TYPE DECLARATION //
#define LSW_TYPE_LED lsw_led_get_type()
LS_WIDGETS_EXPORT G_DECLARE_FINAL_TYPE(LswLed, lsw_led, LSW, LED, GtkWidget)


// PROTOTYPES //
LS_WIDGETS_EXPORT LswLed*
lsw_led_new();

LS_WIDGETS_EXPORT gboolean
lsw_led_get_state(LswLed* self);

LS_WIDGETS_EXPORT void
lsw_led_set_state(  LswLed*     self,
                    gboolean    state   );

LS_WIDGETS_EXPORT void
lsw_led_get_color_on(   LswLed*     self,
                        GdkRGBA*    out     );

LS_WIDGETS_EXPORT void
lsw_led_set_color_on(   LswLed*     self,
                        GdkRGBA*    in      );

LS_WIDGETS_EXPORT void
lsw_led_get_color_off(  LswLed*     self,
                        GdkRGBA*    out     );

LS_WIDGETS_EXPORT void
lsw_led_set_color_off(  LswLed*     self,
                        GdkRGBA*    in      );


#endif