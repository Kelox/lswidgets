#ifndef LSW_BUILDER_H
#define LSW_BUILDER_H


/* This class should behave similar to GtkBuilder.
 * Object ID's will be modified. You can access them
 * similar to how you write file paths:
 *      Root-UI: "my-button" -> "/my-button"
 *      Child-UI "child1": "some-entry" -> "/child1/some-entry"
 *      Child-UI "child2" (used within "child1"): "some-grid" -> "/child1/child2/some-grid"
 *
 * To reference another UI-definition, you simply need this tag:
 *      <LswUI id="my-child" source="/path/to/file.ui" />
 *
 * You can also parse an ui-definition that's already loaded in memory:
 *      <LswUI id="my-child" source="source-id" />
 * In this case it is required to copy this data into the LswBuilder instance beforehand:
 *      lsw_builder_set_source("source-id", some_char_buffer);
 *
 * This class barely does any error checking and will probably break if not used correctly.
 * LswUI-ID's must not contain path separators.
 * */


// INCLUDES //
#include <gtk/gtk.h>
#include <LsWidgets/lsw.h>


// TYPE DECLARATION //
#define LSW_TYPE_BUILDER lsw_builder_get_type()
G_DECLARE_FINAL_TYPE(LswBuilder, lsw_builder, LSW, BUILDER, GObject);


// PROTOTYPES //

/// @brief Creates a new builder.
/// Caller takes ownership
/// @param ui_definition Either a file path or a preloaded string
/// @param is_file Whether ui_definition is a file path or not
LS_WIDGETS_EXPORT LswBuilder*
lsw_builder_new(    const char*     ui_definition,
                    gboolean        is_file         );

/// @brief Saves an ui-definition within a dictionary. This should be
/// used if the .ui-file was already loaded into memory.
/// See the explanation above.
/// @param source_id The ID that was used in the LswBuilder.source attribute.
/// The ID "root" is reserved. Do not use it.
/// @param ui_definition The ui definition that should be copied
LS_WIDGETS_EXPORT void
lsw_builder_set_source( LswBuilder*     self,
                        const char*     source_id,
                        const char*     ui_definition   );

/// @brief Parses the ui-files and returns the resulting GtkBuilder
/// that can be used as usual.
LS_WIDGETS_EXPORT GtkBuilder*
lsw_builder_make(LswBuilder* self);

/// @brief Same as lsw_builder_make, but you get the resulting
/// ui-file as a string.
/// Probably only useful for debugging purposes.
/// @return ui-definition as string.
/// The caller owns the data.
LS_WIDGETS_EXPORT char*
lsw_builder_make_string(LswBuilder* self);


#endif