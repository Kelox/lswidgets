#ifndef LSW_GRAPH_DATA_H
#define LSW_GRAPH_DATA_H


// INCLUDES //
#include <glib-object.h>
#include <LsWidgets/lsw_export.h>


// ENUMS //
enum LswGraphDataType
{
    LSW_GRAPH_DATA_TYPE_NONE = 0,
    LSW_GRAPH_DATA_TYPE_INT,
    LSW_GRAPH_DATA_TYPE_BYTE,
    LSW_GRAPH_DATA_TYPE_FLOAT,
    LSW_GRAPH_DATA_TYPE_DOUBLE
};


// STRUCTS //
typedef struct
{
    enum LswGraphDataType data_type;
    gboolean is_2d; // [ x, y, x, y, x, ... ]
} lsw_gd_config_t;


// TYPE DECLARATION //
#define LSW_TYPE_GRAPH_DATA lsw_graph_data_get_type()
LS_WIDGETS_EXPORT G_DECLARE_FINAL_TYPE(LswGraphData, lsw_graph_data, LSW, GRAPH_DATA, GObject)


// PROTOTYPES //

LS_WIDGETS_EXPORT LswGraphData*
lsw_graph_data_new();

/// @brief Sets the new data.
/// The old data will be freed, if there was any.
/// @param data Pointer to the data, it must be packed.
/// (eg. either [y, y, y, ...] or [x, y, x, y, ...])
/// @param data_length Length of the data array (eg. float[3] would be 3)
/// @param data_type The type of pointer
/// @param is_2d Whether it is [y, y, y, ...] or [x, y, x, y, ...].
/// If it is 1d, the indices are used as the x-values.
LS_WIDGETS_EXPORT void
lsw_graph_data_set_data(    LswGraphData*           self,
                            void*                   data,
                            int                     data_length,
                            enum LswGraphDataType   data_type,
                            gboolean                is_2d       );

/// @brief Borrows the data. Blocks until its available.
/// It must be returned using lsw_graph_data_return(..) later.
/// If there is no data it returns NULL. You don't have to
/// return it in this case.
LS_WIDGETS_EXPORT void
lsw_graph_data_borrow(    LswGraphData*   self,
                          void**          data,
                          int*            data_length );

/// @brief Returns the borrowed data.
/// This also sets your pointer to NULL.
/// You MUST NOT use the data afterwards!
LS_WIDGETS_EXPORT void
lsw_graph_data_return(  LswGraphData*   self,
                        void**          data    );

/// @brief Takes ownership of the previously borrowed data.
/// This GraphData then contains "NULL" and is released as if
/// lsw_graph_data_return() was called.
/// You may use this for resizing/swapping the data.
LS_WIDGETS_EXPORT void
lsw_graph_data_take(    LswGraphData*   self,
                        void*           data    );

/// @brief Writes the points into the user-specified arrays.
/// The data must've been borrowed first. Borrow-checking is only
/// active within debug-builds.
/// @param data The borrowed data
/// @param r_resolution The minimum distance between points
/// @param r_start_x The range start
/// @param r_end_x The range end
/// @param out_x Output array of x-values
/// @param out_y Output array of y-values
/// @param out_length The length of the output array
/// @return The amount of points written into the out-arrays.
/// If this value == out_length, not all points might have fit into
/// the arrays.
LS_WIDGETS_EXPORT int
lsw_graph_data_get_from_range(  LswGraphData*   self,
                                void*           data,
                                float           r_resolution,
                                float           r_start_x,
                                float           r_end_x,
                                float*          out_x,
                                float*          out_y,
                                int             out_length      );


#endif