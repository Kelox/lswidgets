#ifndef LSW_IMAGE_H
#define LSW_IMAGE_H

// INCLUDES //
#include <gtk/gtk.h>
#include <LsWidgets/lsw.h>

// TYPE DECLARATION //
#define LSW_TYPE_IMAGE lsw_image_get_type()
LS_WIDGETS_EXPORT G_DECLARE_FINAL_TYPE(LswImage, lsw_image, LSW, IMAGE, GtkWidget)

// METHOD PROTOTYPES //

/// @brief Creates a new empty image widget
LS_WIDGETS_EXPORT LswImage*
lsw_image_new();

/// @brief Removes the image and frees its resources
LS_WIDGETS_EXPORT void
lsw_image_clear(LswImage* self);

/// @brief Sets the image
/// @param fpath Path to a .png file. The file must exist.
LS_WIDGETS_EXPORT void
lsw_image_set_png(  LswImage*   self,
                    const char* fpath   );

/// @brief Sets the image by copying the specified buffer.
/// The buffer is expected to have this format:
/// [ r, g, b, a, r, g, b, a, r, ... ].
/// Thus, the buffer is required to have a length of
/// (buf_width * buf_height * 4).
LS_WIDGETS_EXPORT void
lsw_image_set_rgba( LswImage*       self,
                    const guchar*   buffer,
                    int             buf_width,
                    int             buf_height  );

/// @brief Same as lsw_image_set_rgba, but with only gray
/// values. eg. this format:
/// [ g, g, g, ... ].
/// Thus, the buffer is required to have a length of
/// (buf_width * buf_height).
LS_WIDGETS_EXPORT void
lsw_image_set_gray( LswImage*       self,
                    const guchar*   buffer,
                    int             buf_width,
                    int             buf_height  );


LS_WIDGETS_EXPORT void
lsw_image_set_min_scale(    LswImage*   self,
                            float       p_scale );

LS_WIDGETS_EXPORT float
lsw_image_get_min_scale(LswImage* self);

LS_WIDGETS_EXPORT void
lsw_image_set_scale(    LswImage*   self,
                        float       p_scale );

LS_WIDGETS_EXPORT float
lsw_image_get_scale(LswImage* self);


#endif