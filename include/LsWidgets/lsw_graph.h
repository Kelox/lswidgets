#ifndef LSW_GRAPH_H
#define LSW_GRAPH_H


// INCLUDES //
#include <gtk/gtk.h>
#include <LsWidgets/lsw_export.h>
#include <LsWidgets/lsw_graph_axis.h>
#include <LsWidgets/lsw_graph_data.h>


// ENUMS //
enum LswSimpleGraphType
{
    LSW_GRAPH_TYPE_DOTS = 0,
    LSW_GRAPH_TYPE_LINES,
    LSW_GRAPH_TYPE_BARS,
};


// TYPE DECLARATION //
#define LSW_TYPE_GRAPH lsw_graph_get_type()
LS_WIDGETS_EXPORT G_DECLARE_FINAL_TYPE(LswGraph, lsw_graph, LSW, GRAPH, GtkWidget)


// PROTOTYPES //
LS_WIDGETS_EXPORT LswGraph*
lsw_graph_new();

/// @brief Adds a new dataset to display. This won't draw until
/// the axis properties are created.
/// @param data Increases the refcount. You must unref manually if you
/// do not need it anymore.
LS_WIDGETS_EXPORT void
lsw_graph_add_data( LswGraph*       self,
                    LswGraphData*   data,
                    gboolean        axis_left,
                    gboolean        axis_bottom,
                    GdkRGBA         color       );

LS_WIDGETS_EXPORT void
lsw_graph_remove_data(  LswGraph*       self,
                        LswGraphData*   data    );


#endif