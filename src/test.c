#include <LsWidgets/lsw.h>
#include <math.h>
#include <stdlib.h>

static LswLed* led;


static void
on_button_clicked(  GtkButton*  self,
                    gpointer    user_data   )
{
    lsw_led_set_state(led, !lsw_led_get_state(led));
}


static void
activate(   GtkApplication* app,
            gpointer        user_data)
{
    ls_widgets_init();
    
    GtkWidget* window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Lsw Test");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    
    led = lsw_led_new();
    GtkWidget* button = gtk_button_new_with_label("test");
    g_signal_connect(button, "clicked", G_CALLBACK(on_button_clicked), NULL);
    
    GtkBox* box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    gtk_box_append(box, GTK_WIDGET(led));
    gtk_box_append(box, button);
    
    GtkBox* smbox = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    GtkBuilder* builder = gtk_builder_new_from_file("data/LsWidgets/StateMachineTemplate.ui");
    GtkWidget* builder_widget = GTK_WIDGET(gtk_builder_get_object(builder, "Root"));
    gtk_box_append(smbox, builder_widget);
    
    GtkBuilder* builder2 = gtk_builder_new_from_file("data/LsWidgets/StateMachineTemplate.ui");
    GtkWidget* builder_widget2 = GTK_WIDGET(gtk_builder_get_object(builder2, "Root"));
    gtk_box_append(smbox, builder_widget2);
    
    //gtk_box_append(box, GTK_WIDGET(smbox));
    
    gtk_window_set_child(GTK_WINDOW(window), GTK_WIDGET(box));
    
    
    LswBuilder* lswbuilder = lsw_builder_new("data/LsWidgets/LswBuilderExample.ui", TRUE);
    GtkBuilder* result_builder = lsw_builder_make(lswbuilder);
    g_object_unref(lswbuilder);
    lswbuilder = NULL;
    
    gtk_box_append(box, GTK_WIDGET(gtk_builder_get_object(result_builder, "/RootGrid")));
    
    gtk_window_present(GTK_WINDOW(window));
    
    GObject* object = g_object_new(G_TYPE_OBJECT, NULL);
    g_object_unref(object);
    
    LswGraphData* gdata = lsw_graph_data_new();
    
    float* ph = (float*)malloc(sizeof(float) * 1000000);
    for (int i = 0; i < 1000000; i++)
    {
        float x = (3.141f / 64.0f) * (float)i;
        float y = sinf(x);
        ph[i] = y;
    }
    
    lsw_graph_data_set_data(gdata, ph, 1000000, LSW_GRAPH_DATA_TYPE_FLOAT, FALSE);
    ph = NULL;
    
    int phlen = 0;
    lsw_graph_data_borrow(gdata, (void**)&ph, &phlen);
    
    float subx[256];
    float suby[256];
    int read_len = lsw_graph_data_get_from_range(
        gdata,
        (void*)ph,
        6.0f, 0, 1000000.0f,
        subx, suby,
        256
    );
    lsw_graph_data_return(gdata, (void*)&ph);
    LswGraph* graph = LSW_GRAPH(
        gtk_builder_get_object(result_builder, "/graph")
    );
    lsw_graph_add_data(graph, gdata, TRUE, TRUE, (GdkRGBA) { });
    
    LswGraphAxis* axis = lsw_graph_axis_new();
    lsw_graph_axis_set_display_min_max(axis, 0, 1.0f);
    GValue value = G_VALUE_INIT;
    g_value_init(&value, LSW_TYPE_GRAPH_AXIS);
    g_value_set_object(&value, axis);
    g_object_set_property(
        G_OBJECT(graph), "axis-top", &value
    );
    
    LswGraphAxis* vaxis = lsw_graph_axis_new();
    lsw_graph_axis_set_display_min_max(vaxis, 0, 256.0f);
    GValue v2 = G_VALUE_INIT;
    g_value_init(&v2, LSW_TYPE_GRAPH_AXIS);
    g_value_set_object(&v2, vaxis);
    g_object_set_property(
        G_OBJECT(graph), "axis-left", &v2
    );
    
    LswImage* image = lsw_image_new();
    lsw_image_set_png(image, "/home/cookie/Pictures/test.png");
    lsw_image_set_scale(image, 0.5f);
    lsw_image_set_min_scale(image, 0.0f);
    gtk_box_append(
        GTK_BOX(gtk_builder_get_object(result_builder, "/listbox")),
        GTK_WIDGET(image)
    );
    
    g_object_unref(gdata);
}


int
main(   int     argc,
        char**  argv    )
{
    GtkApplication* app = gtk_application_new(
        "com.test.test",
        G_APPLICATION_DEFAULT_FLAGS
    );
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    int status = g_application_run(G_APPLICATION(app), argc, argv);
    ls_widgets_cleanup();
    g_object_unref(app);
    
    return status;
}