#include <LsWidgets/lsw_image.h>
#include <stdlib.h>

#define MIN_SIZE 32


// PROPERTIES //
enum LswImageProperties
{
    PROP_MIN_SCALE = 1,
    PROP_SCALE,
    
    N_PROPERTIES
};
static GParamSpec* props[N_PROPERTIES] = { NULL };


// PROTOTYPES //
static void
lsw_image_dispose(GObject* self_object);

static void
lsw_image_finalize(GObject* self_object);

static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget);

static void
measure(    GtkWidget*      self_widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline    );

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap            );

static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       );

static void
get_property(   GObject*        self_object,
                guint           property_id,
                GValue*         value,
                GParamSpec*     pspec       );

void
lsw_image_set_empty(    LswImage*   self,
                        int         width,
                        int         height  );


// OBJECT CONSTRUCTION //
struct _LswImage
{
    GtkWidget parent;
    
    gboolean disposed;
    
    float scale, min_scale;
    
    int img_width, img_height;
    cairo_surface_t* img_surface;
};
G_DEFINE_TYPE(LswImage, lsw_image, GTK_TYPE_WIDGET)

static void
lsw_image_class_init(LswImageClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &lsw_image_dispose;
    G_OBJECT_CLASS(klass)->finalize = &lsw_image_finalize;
    G_OBJECT_CLASS(klass)->set_property = &set_property;
    G_OBJECT_CLASS(klass)->get_property = &get_property;

    GTK_WIDGET_CLASS(klass)->get_request_mode = &get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = &measure;
    GTK_WIDGET_CLASS(klass)->snapshot = &snapshot;
    
    // Install properties
    props[PROP_MIN_SCALE] = g_param_spec_float(
        "min-scale",
        "MinScale",
        "Minimum scale that this image should display with",
        0.0f, 1.0f, 0.5f,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_SCALE] = g_param_spec_float(
        "scale",
        "Scale",
        "The scale this widget should naturally display with",
        0.0f, 1.0f, 1.0f,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    
    g_object_class_install_properties(
        G_OBJECT_CLASS(klass), N_PROPERTIES, props
    );
}

static void
lsw_image_init(LswImage* self)
{
    self->disposed = FALSE;
    
    self->scale = 1.0f;
    self->min_scale = 0.5f;
    
    self->img_width = -1;
    self->img_height = -1;
    self->img_surface = NULL;
}


// OBJECT DECONSTRUCTION //
static void
lsw_image_dispose(GObject* self_object)
{
    LswImage* self = LSW_IMAGE(self_object);
    if (!self->disposed)
    {
        lsw_image_clear(self);
        
        self->disposed = TRUE;
    }
    
    // Chain dispose
    G_OBJECT_CLASS(lsw_image_parent_class)->dispose(self_object);
}

static void
lsw_image_finalize(GObject* self_object)
{
    LswImage* self = LSW_IMAGE(self_object);
    
    // Chain finalize
    G_OBJECT_CLASS(lsw_image_parent_class)->finalize(self_object);
}


// GOBJECT METHODS //
static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       )
{
    LswImage* self = LSW_IMAGE(self_object);
    
    switch (property_id)
    {
        case PROP_MIN_SCALE:
            self->min_scale = g_value_get_float(value);
            break;
        case PROP_SCALE:
            self->scale = g_value_get_float(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}

static void
get_property(   GObject*        self_object,
                guint           property_id,
                GValue*         value,
                GParamSpec*     pspec       )
{
    LswImage* self = LSW_IMAGE(self_object);
    
    switch (property_id)
    {
        case PROP_MIN_SCALE:
            g_value_set_float(value, self->min_scale);
            break;
        case PROP_SCALE:
            g_value_set_float(value, self->scale);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}


// WIDGET METHODS //
static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget)
{
    return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
measure(    GtkWidget*      self_widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline    )
{
    LswImage* self = LSW_IMAGE(self_widget);
    
    if (orientation == GTK_ORIENTATION_VERTICAL)
    {
        // Get height for given width
        *minimum = (int)((float)self->img_height * self->scale * self->min_scale);
        *natural = (int)((float)self->img_height * self->scale);
    }
    else
    {
        // Get width for given height
        *minimum = (int)((float)self->img_width * self->scale * self->min_scale);
        *natural = (int)((float)self->img_width * self->scale);
    }
    
    *minimum_baseline = -1;
    *natural_baseline = -1;
    
    if (*minimum < MIN_SIZE)
        *minimum = MIN_SIZE;
    
    if (*natural < *minimum)
        *natural = *minimum;
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap            )
{
    LswImage* self = LSW_IMAGE(self_widget);
    
    // Return if there is no image to show
    if (!self->img_surface)
        return;
    
    // Get widget size
    GtkAllocation alloc;
    gtk_widget_get_allocation(self_widget, &alloc);
    
    // Get image size within the widget
    int height = alloc.height;
    int width = (int)(((float)self->img_width / (float)self->img_height) * (float)height);
    
    if (width > alloc.width)
    {
        width = alloc.width;
        height = (int)(((float)self->img_height / (float)self->img_width) * (float)width);
    }
    
    // Prepare cairo
    cairo_t* cairo = gtk_snapshot_append_cairo(
        snap,
        &GRAPHENE_RECT_INIT(
                (alloc.width - width) / 2, (alloc.height - height) / 2,
                width, height
        )
    );
    
    cairo_translate(
        cairo,
        (double)(alloc.width - width) / 2.0,
        (double)(alloc.height - height) / 2.0
    );
    double scale = (double)height / (double)self->img_height;
    cairo_scale(cairo, scale, scale);
    
    // Draw image
    cairo_set_source_surface(cairo, self->img_surface, 0, 0);
    cairo_paint(cairo);
    
    // Clean up
    cairo_destroy(cairo);
}


// PUBLIC METHODS //
LswImage*
lsw_image_new()
{
    LswImage* self = LSW_IMAGE(g_object_new(LSW_TYPE_IMAGE, NULL));
    
    return self;
}

void
lsw_image_clear(LswImage* self)
{
    if (!self->img_surface)
        return;
    
    cairo_surface_finish(self->img_surface);
    cairo_surface_destroy(self->img_surface);
    self->img_surface = NULL;
    self->img_width = -1;
    self->img_height = -1;
    gtk_widget_queue_draw(GTK_WIDGET(self));
}

void
lsw_image_set_png(  LswImage*   self,
                    const char* fpath   )
{
    // Delete old image
    lsw_image_clear(self);
    
    // Check if there is no path
    int pathlen = (int)strlen(fpath);
    if (pathlen == 0)
        g_error("File path is empty");
    
    // Test if file exists
    gboolean testres = g_file_test(
        fpath,
        G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR
    );
    if (!testres)
        g_error("The file '%s' does not exist", fpath);
    
    // Check file type
    gchar* temp_path = g_utf8_strup(fpath, pathlen);
    int ext_idx;
    for (ext_idx = pathlen - 1; ext_idx >= 0; ext_idx--)
    {
        if (temp_path[ext_idx] == '.')
            break;
    }
    if (strcmp(&temp_path[ext_idx], ".PNG") != 0)
        g_error("The file '%s' is not a .png-file", fpath);
    g_free(temp_path);
    
    // Create image surface
    self->img_surface = cairo_image_surface_create_from_png(fpath);
    if (!self->img_surface)
        g_error("Failed to read '%s'", fpath);
    self->img_width = cairo_image_surface_get_width(self->img_surface);
    self->img_height = cairo_image_surface_get_height(self->img_surface);
    if (self->img_width <= 0 || self->img_height <= 0)
        g_error("Failed to read '%s'", fpath);
    
    gtk_widget_queue_draw(GTK_WIDGET(self));
}

void
lsw_image_set_rgba( LswImage*       self,
                    const guchar*   buffer,
                    int             buf_width,
                    int             buf_height  )
{
    lsw_image_set_empty(self, buf_width, buf_height);
    
    memcpy(
        cairo_image_surface_get_data(self->img_surface),
        buffer,
        self->img_width * self->img_height * 4
    );
    
    cairo_surface_mark_dirty(self->img_surface);
    gtk_widget_queue_draw(GTK_WIDGET(self));
}

void
lsw_image_set_gray( LswImage*       self,
                    const guchar*   buffer,
                    int             buf_width,
                    int             buf_height  )
{
    lsw_image_set_empty(self, buf_width, buf_height);
    
    unsigned char* img = cairo_image_surface_get_data(self->img_surface);
    for (int y = 0; y < self->img_height; y++)
    {
        for (int x = 0; x < self->img_width; x++)
        {
            int idx = x + y * self->img_width;
            img[idx * 4] = buffer[idx];
            img[idx * 4 + 1] = buffer[idx];
            img[idx * 4 + 2] = buffer[idx];
            img[idx * 4 + 3] = 255;
        }
    }
    // TODO: maybe use something similar to parallel_for to speed this up
    
    cairo_surface_mark_dirty(self->img_surface);
    gtk_widget_queue_draw(GTK_WIDGET(self));
}


void
lsw_image_set_min_scale(    LswImage*   self,
                            float       p_scale )
{
    g_object_set(G_OBJECT(self), "min-scale", p_scale, NULL);
}

float
lsw_image_get_min_scale(LswImage* self)
{
    float value;
    g_object_get(G_OBJECT(self), "min-scale", &value, NULL);
    return value;
}

void
lsw_image_set_scale(    LswImage*   self,
                        float       p_scale )
{
    g_object_set(G_OBJECT(self), "scale", p_scale, NULL);
}

float
lsw_image_get_scale(LswImage* self)
{
    float value;
    g_object_get(G_OBJECT(self), "scale", &value, NULL);
    return value;
}



// PRIVATE METHODS //
void
lsw_image_set_empty(    LswImage*   self,
                        int         width,
                        int         height  )
{
    // Create new surface if necessary
    if (self->img_surface && (self->img_width != width || self->img_height != height))
    {
        // Clear old surface
        lsw_image_clear(self);
        
        // Create new surface
        self->img_surface = cairo_image_surface_create(
            CAIRO_FORMAT_ARGB32, width, height
        );
        self->img_width = width;
        self->img_height = height;
    }
}