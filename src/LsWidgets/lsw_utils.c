#include <LsWidgets/lsw_utils.h>


// VARIABLES //
static gboolean is_dark = FALSE;


// PUBLIC FUNCTIONS //
void lsw_utils_set_dark_mode(gboolean p_is_dark)
{
    if (p_is_dark)
        g_info("Changed system theme to DARK");
    else
        g_info("Changed system theme to LIGHT");
    is_dark = p_is_dark;
}

gboolean lsw_utils_get_dark_mode()
{
    return is_dark;
}