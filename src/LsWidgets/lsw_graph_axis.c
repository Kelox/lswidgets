#include <LsWidgets/lsw_graph_axis.h>


// ENUMS //
enum LswGraphAxisProperties
{
    PROP_DISPLAY_MIN = 1,
    PROP_DISPLAY_MAX,
    
    N_PROPERTIES
};
static GParamSpec* props[N_PROPERTIES] = { NULL };


// PROTOTYPES //
static void
dispose(GObject* self_object);

static void
finalize(GObject* self_object);


static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       );

static void
get_property(   GObject*    self_object,
                guint       property_id,
                GValue*     value,
                GParamSpec* pspec       );


// TYPE DEFINITION //
struct _LswGraphAxis
{
    GObject parent;
    
    gboolean disposed;
    float display_min, display_max; // visible display range
};
G_DEFINE_FINAL_TYPE(LswGraphAxis, lsw_graph_axis, G_TYPE_OBJECT);


// CONSTRUCTOR / DECONSTRUCTOR //
static void
lsw_graph_axis_class_init(LswGraphAxisClass* klass)
{
    // Overwrite virtual functions
    G_OBJECT_CLASS(klass)->dispose = &dispose;
    G_OBJECT_CLASS(klass)->finalize = &finalize;
    G_OBJECT_CLASS(klass)->set_property = &set_property;
    G_OBJECT_CLASS(klass)->get_property = &get_property;
    
    // Install properties
    props[PROP_DISPLAY_MIN] = g_param_spec_float(
        "display-min",
        "DisplayMin",
        "Visible range minimum that should be displayed",
        -999999999.0f, 999999999.0f,
        0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_DISPLAY_MAX] = g_param_spec_float(
        "display-max",
        "DisplayMax",
        "Visible range maximum that should be displayed",
        -999999999.0f, 999999999.0f,
        0,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    
    g_object_class_install_properties(
        G_OBJECT_CLASS(klass),
        N_PROPERTIES,
        props
    );
}

static void
lsw_graph_axis_init(LswGraphAxis* self)
{
    self->disposed = FALSE;
    self->display_min = 0;
    self->display_max = 0;
}

static void
dispose(GObject* self_object)
{
    LswGraphAxis* self = LSW_GRAPH_AXIS(self_object);
    
    if (!self->disposed)
    {
        
        self->disposed = TRUE;
    }
    
    // Chain
    G_OBJECT_CLASS(lsw_graph_axis_parent_class)->dispose(self_object);
}

static void
finalize(GObject* self_object)
{
    LswGraphAxis* self = LSW_GRAPH_AXIS(self_object);
    
    // Chain
    G_OBJECT_CLASS(lsw_graph_axis_parent_class)->finalize(self_object);
}


// PUBLIC FUNCTIONS //
LswGraphAxis*
lsw_graph_axis_new()
{
    LswGraphAxis* self = LSW_GRAPH_AXIS(g_object_new(
        LSW_TYPE_GRAPH_AXIS,
        "display-min", 0.0f,
        "display-max", 0.0f,
        NULL
    ));
    return self;
}

void
lsw_graph_axis_set_display_min( LswGraphAxis*   self,
                                float           minv    )
{
    g_object_set(G_OBJECT(self), "display-min", minv, NULL);
}
float
lsw_graph_axis_get_display_min(LswGraphAxis* self)
{
    float minv;
    g_object_get(G_OBJECT(self), "display-min", &minv, NULL);
    return minv;
}

void
lsw_graph_axis_set_display_max( LswGraphAxis*   self,
                                float           maxv    )
{
    g_object_set(G_OBJECT(self), "display-max", maxv, NULL);
}
float
lsw_graph_axis_get_display_max(LswGraphAxis* self)
{
    float maxv;
    g_object_get(G_OBJECT(self), "display-max", &maxv, NULL);
    return maxv;
}

void
lsw_graph_axis_set_display_min_max( LswGraphAxis*   self,
                                    float           minv,
                                    float           maxv    )
{
    lsw_graph_axis_set_display_min(self, minv);
    lsw_graph_axis_set_display_max(self, maxv);
    
}
void
lsw_graph_axis_get_display_min_max( LswGraphAxis*   self,
                                    float*          minv,
                                    float*          maxv    )
{
    *minv = lsw_graph_axis_get_display_min(self);
    *maxv = lsw_graph_axis_get_display_max(self);
}



// PRIVATE FUNCTIONS //
static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       )
{
    LswGraphAxis* self = LSW_GRAPH_AXIS(self_object);
    
    switch (property_id)
    {
        case PROP_DISPLAY_MIN:
        {
            float v = g_value_get_float(value);
            self->display_min = v;
            break;
        }
        case PROP_DISPLAY_MAX:
        {
            float v = g_value_get_float(value);
            self->display_max = v;
            break;
        }
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}

static void
get_property(   GObject*    self_object,
                guint       property_id,
                GValue*     value,
                GParamSpec* pspec       )
{
    LswGraphAxis* self = LSW_GRAPH_AXIS(self_object);
    
    switch (property_id)
    {
        case PROP_DISPLAY_MIN:
            g_value_set_float(value, self->display_min);
            break;
        case PROP_DISPLAY_MAX:
            g_value_set_float(value, self->display_max);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}