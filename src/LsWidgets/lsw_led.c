#include "LsWidgets/lsw_led.h"


// MACROS //
#define LED_WIDTH 48
#define LED_HEIGHT (LED_WIDTH * 0.75)
#define LED_SPACE 4


// PROTOTYPES //
static void
dispose(GObject* self_object);

static void
finalize(GObject* self_object);

static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       );

static void
get_property(   GObject*        self_object,
                guint           property_id,
                GValue*         value,
                GParamSpec*     pspec       );

static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget);

static void
measure(
    GtkWidget*      widget,
    GtkOrientation  orientation,
    int             for_size,
    int*            minimum,
    int*            natural,
    int*            minimum_baseline,
    int*            natural_baseline    );

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap        );


// ENUMS //
enum LswLedProperties
{
    PROP_LED_STATE = 1,
    PROP_LED_COLOR_ON,
    PROP_LED_COLOR_OFF,
    N_PROPERTIES
};


// VARIABLES //
static GParamSpec* props[N_PROPERTIES] = { NULL };


// OBJECT CONSTRUCTION //
struct _LswLed
{
    GtkWidget parent;
    
    // Properties
    gboolean prop_led_state;
    GString* prop_led_color_on;
    GString* prop_led_color_off;
    
    GskColorStop lin_stop[4];
    GskColorStop rad_stop[3];
};
G_DEFINE_TYPE(LswLed, lsw_led, GTK_TYPE_WIDGET)

static void
lsw_led_class_init(LswLedClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &dispose;
    G_OBJECT_CLASS(klass)->finalize = &finalize;
    G_OBJECT_CLASS(klass)->set_property = &set_property;
    G_OBJECT_CLASS(klass)->get_property = &get_property;
    GTK_WIDGET_CLASS(klass)->get_request_mode = &get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = &measure;
    GTK_WIDGET_CLASS(klass)->snapshot = &snapshot;
    
    
    // Install properties
    props[PROP_LED_STATE] = g_param_spec_boolean(
        "state",
        "State",
        "Controls the light state",
        FALSE,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_LED_COLOR_ON] = g_param_spec_string(
        "color-on",
        "ColorOn",
        "Color which is formatted like this: \"#RRGGBBAA\"",
        "#336633FF",
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_LED_COLOR_OFF] = g_param_spec_string(
        "color-off",
        "ColorOff",
        "Color which is formatted like this: \"#RRGGBBAA\"",
        "#66FF66FF",
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    g_object_class_install_properties(
        G_OBJECT_CLASS(klass),
        N_PROPERTIES,
        props
    );
    
    // FIXME: DO I HAVE TO FREE props ??
}

static void
lsw_led_init(LswLed* self)
{
    gtk_widget_set_hexpand(GTK_WIDGET(self), FALSE);
    gtk_widget_set_vexpand(GTK_WIDGET(self), FALSE);
    
    // Init properties
    self->prop_led_state = FALSE;
    self->prop_led_color_on = g_string_new("#33FF33FF");
    self->prop_led_color_off = g_string_new("#FF3333FF");
    
    // Init gradient color stops
    self->lin_stop[0].offset = 0;
    self->lin_stop[0].color =
        (GdkRGBA){ 0.6f, 0.6f, 0.6f, 1.0f };
    self->lin_stop[1].offset = 0.5f;
    self->lin_stop[1].color =
        (GdkRGBA){ 0.5f, 0.5f, 0.5f, 1.0f };
    self->lin_stop[2].offset = 0.5f;
    self->lin_stop[2].color =
        (GdkRGBA){ 0.4f, 0.4f, 0.4f, 1.0f };
    self->lin_stop[3].offset = 1.0f;
    self->lin_stop[3].color =
        (GdkRGBA){ 0.3f, 0.3f, 0.3f, 1.0f };
    
    self->rad_stop[0].offset = 0;
    self->rad_stop[0].color =
        (GdkRGBA){ 1.0f, 1.0f, 1.0f, 0.6f };
    self->rad_stop[1].offset = 0.2f;
    self->rad_stop[1].color =
        (GdkRGBA){ 1.0f, 1.0f, 1.0f, 0.55f };
    self->rad_stop[2].offset = 0.75f;
    self->rad_stop[2].color =
        (GdkRGBA){ 1.0f, 1.0f, 1.0f, 0.0f };
}


// OBJECT DECONSTRUCTION //
static void
dispose(GObject* self_object)
{
    // ...
    
    // Chain dispose
    G_OBJECT_CLASS(lsw_led_parent_class)->dispose(self_object);
}

static void
finalize(GObject* self_object)
{
    LswLed* self = LSW_LED(self_object);
    
    g_string_free(self->prop_led_color_on, true);
    g_string_free(self->prop_led_color_off, true);
    
    // Chain finalize
    G_OBJECT_CLASS(lsw_led_parent_class)->finalize(self_object);
}


// GOBJECT VIRTUAL FUNCTIONS //
static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       )
{
    LswLed* self = LSW_LED(self_object);
    
    switch (property_id)
    {
        case PROP_LED_STATE:
            self->prop_led_state = g_value_get_boolean(value);
            break;
        case PROP_LED_COLOR_ON:
            g_string_erase(
                self->prop_led_color_on,
                0, (gssize)self->prop_led_color_on->len
            );
            g_string_append(
                self->prop_led_color_on,
                g_value_get_string(value)
            );
            break;
        case PROP_LED_COLOR_OFF:
            g_string_erase(
                self->prop_led_color_off,
                0, (gssize)self->prop_led_color_off->len
            );
            g_string_append(
                self->prop_led_color_off,
                g_value_get_string(value)
            );
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
    }
}

static void
get_property(   GObject*        self_object,
                guint           property_id,
                GValue*         value,
                GParamSpec*     pspec       )
{
    LswLed* self = LSW_LED(self_object);
    
    switch (property_id)
    {
        case PROP_LED_STATE:
            g_value_set_boolean(value, self->prop_led_state);
            break;
        case PROP_LED_COLOR_ON:
            g_value_set_string(value, self->prop_led_color_on->str);
            break;
        case PROP_LED_COLOR_OFF:
            g_value_set_string(value, self->prop_led_color_off->str);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
    }
}


// GTKWIDGET VIRTUAL FUNCTIONS //
static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget)
{
    return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
measure(
    GtkWidget*      widget,
    GtkOrientation  orientation,
    int             for_size,
    int*            minimum,
    int*            natural,
    int*            minimum_baseline,
    int*            natural_baseline    )
{
    if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
        *minimum = LED_WIDTH;
        *natural = LED_WIDTH;
    }
    else
    {
        *minimum = LED_HEIGHT;
        *natural = LED_HEIGHT;
    }
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap        )
{
    LswLed* self = LSW_LED(self_widget);
    
    GtkAllocation alloc;
    gtk_widget_get_allocation(self_widget, &alloc);
    gtk_snapshot_translate(
        snap,
        &GRAPHENE_POINT_INIT(
            (alloc.width - LED_WIDTH) / 2,
            (alloc.height - LED_HEIGHT) / 2
        )
    );
    
    // LED background
    gtk_snapshot_append_linear_gradient(
        snap,
        &GRAPHENE_RECT_INIT(0, 0, LED_WIDTH, LED_HEIGHT),
        &GRAPHENE_POINT_INIT(0, 0),
        &GRAPHENE_POINT_INIT(LED_WIDTH, LED_HEIGHT),
        self->lin_stop,
        4
    );
    
    
    // LED color
    GdkRGBA led_color;
    if (lsw_led_get_state(self))
        lsw_led_get_color_on(self, &led_color);
    else
        lsw_led_get_color_off(self, &led_color);
    
    gtk_snapshot_append_color(
        snap,
        &led_color,
        &GRAPHENE_RECT_INIT(
            LED_SPACE, LED_SPACE,
            LED_WIDTH - LED_SPACE * 2, LED_HEIGHT - LED_SPACE * 2
        )
    );
    
    // LED gloss
    gtk_snapshot_append_radial_gradient(
        snap,
        &GRAPHENE_RECT_INIT(
            LED_SPACE, LED_SPACE,
            LED_WIDTH - LED_SPACE * 2, LED_HEIGHT - LED_SPACE * 2
        ),
        &GRAPHENE_POINT_INIT(LED_WIDTH / 3, LED_HEIGHT / 3),
        (float)LED_HEIGHT,
        (float)LED_HEIGHT,
        0.0f,
        1.0f,
        self->rad_stop,
        3
    );
}


// PUBLIC FUNCTIONS //
LswLed*
lsw_led_new()
{
    LswLed* self = LSW_LED(
        g_object_new(LSW_TYPE_LED, NULL)
    );
    
    return self;
}

gboolean
lsw_led_get_state(LswLed* self)
{
    gboolean state;
    g_object_get(self, "state", &state, NULL);
    return state;
}

void
lsw_led_set_state(  LswLed*     self,
                    gboolean    state   )
{
    g_object_set(self, "state", state, NULL);
    gtk_widget_queue_draw(GTK_WIDGET(self));
}

void
lsw_led_get_color_on(   LswLed*     self,
                        GdkRGBA*    out     )
{
    gchar* color_str = NULL;
    g_object_get(self, "color-on", &color_str, NULL);
    
    gdk_rgba_parse(out, color_str);
    
    g_free(color_str);
}

void
lsw_led_set_color_on(   LswLed*     self,
                        GdkRGBA*    in      )
{
    gchar color_str[32];
    sprintf(
        color_str,
        "#%02x%02x%02x%02x",
        (guchar)(in->red * 255.0f),
        (guchar)(in->green * 255.0f),
        (guchar)(in->blue * 255.0f),
        (guchar)(in->alpha * 255.0f)
    );
    g_object_set(self, "color-on", color_str, NULL);
}

void
lsw_led_get_color_off(  LswLed*     self,
                        GdkRGBA*    out     )

{
    gchar* color_str = NULL;
    g_object_get(self, "color-off", &color_str, NULL);
    
    gdk_rgba_parse(out, color_str);
    
    g_free(color_str);
}

void
lsw_led_set_color_off(  LswLed*     self,
                        GdkRGBA*    in      )

{
    gchar color_str[32];
    sprintf(
        color_str,
        "#%02x%02x%02x%02x",
        (guchar)(in->red * 255.0f),
        (guchar)(in->green * 255.0f),
        (guchar)(in->blue * 255.0f),
        (guchar)(in->alpha * 255.0f)
    );
    g_object_set(self, "color-off", color_str, NULL);
}