#include <LsWidgets/lsw_builder.h>
#include <stdlib.h>
#ifdef UNIX
#include <libxml2/libxml/xinclude.h>
#include <libxml2/libxml/parser.h>
#else
#include <libxml/xinclude.h>
#include <libxml/parser.h>
#endif


// MACROS //
#define MAX_SOURCE_ID_LENGTH 128
#define MAX_ID_LENGTH 512
#define MARKUP_BUFFER_SIZE 32


// STRUCTS //
typedef struct
{
    char key[MAX_SOURCE_ID_LENGTH];
    void* value;
} lsw_builder_dict;


// PROTOTYPES //
static void
dispose(GObject* self_object);

static void
finalize(GObject* self_object);

/// @brief Reads a file and returns its contents.
/// The caller takes ownership.
static char*
lsw_builder_read_file(  LswBuilder*     self,
                        const char*     file_path   );

static void
lsw_builder_set_source_priv(    LswBuilder*     self,
                                const char*     source_id,
                                const char*     ui_definition,
                                gboolean        copy_data       );

static char*
lsw_builder_get_source( LswBuilder*     self,
                        const char*     source_id   );

static xmlDoc*
lsw_builder_process(    LswBuilder*     self,
                        const char*     buffer,
                        const char*     path    );


// OBJECT CONSTRUCTION //
struct _LswBuilder
{
    GObject parent;
    
    GSList* source_dict;
};
G_DEFINE_TYPE(LswBuilder, lsw_builder, G_TYPE_OBJECT);

static void
lsw_builder_class_init(LswBuilderClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &dispose;
    G_OBJECT_CLASS(klass)->finalize = &finalize;
}

static void
lsw_builder_init(LswBuilder* self)
{

}


// OBJECT DECONSTRUCTION //
static void
dispose(GObject* self_object)
{
    // ...
    
    // Chain dispose
    G_OBJECT_CLASS(lsw_builder_parent_class)->dispose(self_object);
}

static void
finalize(GObject* self_object)
{
    LswBuilder* self = LSW_BUILDER(self_object);
    
    // Free dictionary
    while (g_slist_length(self->source_dict) > 0)
    {
        GSList* link = g_slist_nth(self->source_dict, 0);
        lsw_builder_dict* d = (lsw_builder_dict*)link->data;
        free(d->value);
        free(d);
        link->data = NULL;
        self->source_dict = g_slist_remove_link(self->source_dict, link);
    }
    
    // Chain finalize
    G_OBJECT_CLASS(lsw_builder_parent_class)->finalize(self_object);
}


// PUBLIC METHODS //

LswBuilder*
lsw_builder_new(const char* ui_definition, gboolean is_file)
{
    LswBuilder* self = LSW_BUILDER(g_object_new(LSW_TYPE_BUILDER, NULL));
    self->source_dict = NULL;
    
    if (is_file)
    {
        char* data = lsw_builder_read_file(self, ui_definition);
        lsw_builder_set_source_priv(self, "root", data, FALSE);
    }
    else
    {
        lsw_builder_set_source_priv(self, "root", ui_definition, TRUE);
    }
    
    return self;
}

void
lsw_builder_set_source( LswBuilder*     self,
                        const char*     source_id,
                        const char*     ui_definition   )
{
    lsw_builder_set_source_priv(self, source_id, ui_definition, TRUE);
}

GtkBuilder*
lsw_builder_make(LswBuilder* self)
{
    // Create Builder
    char* buffer = lsw_builder_make_string(self);
    GtkBuilder* builder = gtk_builder_new_from_string(buffer, -1);
    
    // Clean up
    xmlFree(buffer);
    
    // Done
    return builder;
}

char*
lsw_builder_make_string(LswBuilder* self)
{
    // Get doc
    xmlDoc* doc = lsw_builder_process(
        self, lsw_builder_get_source(self, "root"), "/"
    );
    
    // Get buffer
    int size = 0;
    char* buffer = NULL;
    xmlDocDumpMemory(doc, (xmlChar**)&buffer, &size);
    if (!buffer || size < 1)
        g_error("Failed to make GtkBuilder");
    
    // Find start index
    /*int start = 0;
    if (strncmp(buffer, "<?", 2) == 0)
    {
        for (start = 0; ; start++)
        {
            if (strncmp(&buffer[start], "?>", 2) == 0)
                break;
        }
        start += 2;
    }*/
    
    // Clean up
    xmlFreeDoc(doc);
    
    // Done
    return buffer;
}


// PRIVATE METHODS //
static char*
lsw_builder_read_file(  LswBuilder*     self,
                        const char*     file_path   )
{
    // Check if valid
    if (!g_file_test(file_path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
        g_error("Could not find file '%s'", file_path);
    
    // Open
    FILE* file = fopen(file_path, "rb");
    if (!file)
        g_error("Could not open file '%s'", file_path);
    
    // Retrieve file size
    fseek(file, 0, SEEK_END);
    int file_size = (int)ftell(file);
    if (file_size < 1)
        g_error("File '%s' is empty", file_path);
    fseek(file, 0, SEEK_SET);
    
    // Allocate buffer
    char* buffer = (char*)malloc((size_t)file_size + 1);
    if (!buffer)
        g_error("malloc failed");
    int read_bytes = (int)fread(buffer, 1, file_size, file);
    if (read_bytes != file_size)
        g_error("Failed to read '%s': %d/%d bytes read", file_path, read_bytes, file_size);
    buffer[file_size] = '\0';
    
    // Close
    fclose(file);
    return buffer;
}

static void
lsw_builder_set_source_priv(    LswBuilder*     self,
                                const char*     source_id,
                                const char*     ui_definition,
                                gboolean        copy_data       )
{
    // Some basic error checking
    if (!source_id || !ui_definition)
        g_error("source_id and ui_definition must not be NULL");
    if (strlen(source_id) > MAX_SOURCE_ID_LENGTH)
        g_error("source_id exceeds maximum length of %d", MAX_SOURCE_ID_LENGTH);
    
    // Check if this key was already set
    for (int i = 0; i < g_slist_length(self->source_dict); i++)
    {
        lsw_builder_dict* ph = g_slist_nth_data(self->source_dict, i);
        if (strcmp(source_id, ph->key) == 0)
            g_error("source_id '%s' was already set", source_id);
    }
    
    // Create dictionary node
    lsw_builder_dict* dict = (lsw_builder_dict*)malloc(sizeof(lsw_builder_dict));
    if (!dict)
        g_error("malloc failed");
    
    // Set value
    if (copy_data)
    {
        dict->value = malloc(strlen(ui_definition));
        if (!dict->value)
            g_error("malloc failed");
        
        strcpy((char*)dict->value, ui_definition);
    }
    else
    {
        dict->value = (void*)ui_definition;
    }
    
    // Set data
    strcpy(dict->key, source_id);
    
    // Add to list
    self->source_dict = g_slist_append(self->source_dict, dict);
    g_info("LswBuilder: source %s set (%d total)", dict->key, g_slist_length(self->source_dict));
}

static char*
lsw_builder_get_source( LswBuilder*     self,
                        const char*     source_id   )
{
    for (int i = 0; i < g_slist_length(self->source_dict); i++)
    {
        lsw_builder_dict* ph = g_slist_nth_data(self->source_dict, i);
        if (strcmp(source_id, ph->key) == 0)
            return (char*)ph->value;
    }
    return NULL;
}


static void
lsw_builder_process_rename_id(xmlNode* node, const char* path)
{
    if (strcmp((char*)node->name, "object") == 0)
    {
        // Check if it has an ID
        xmlAttr *attr = node->properties;
        while (attr != NULL)
        {
            if (strcmp((char *) attr->name, "id") == 0)
            {
                // Adjust ID to also contain the path
                static char id[MAX_ID_LENGTH];
                snprintf(
                    id, MAX_ID_LENGTH, "%s%s",
                    path, (char*)xmlGetProp(node, (xmlChar*)"id")
                );
                xmlSetProp(node, (xmlChar*)"id", (xmlChar*)id);
                break;
            }
            attr = attr->next;
        }
    }
    
    // Repeat for all children
    xmlNode* child = node->children;
    while (child != NULL)
    {
        lsw_builder_process_rename_id(child, path);
        child = child->next;
    }
}

static xmlNode*
lsw_builder_get_ref_priv(   xmlNode*    node,
                            int*        idx     )
{
    // Is it a ref?
    if (strcmp((char*)node->name, "LswUI") == 0)
    {
        // Return ref if it is the n-th one
        if (*idx == 0)
            return node;
        
        // Otherwise decrease count and continue searching
        *idx--;
    }
    
    // Look through all other children
    xmlNode* child = node->children;
    while (child != NULL)
    {
        xmlNode* result = lsw_builder_get_ref_priv(child, idx);
        if (result)
            return result;
        child = child->next;
    }
    
    return NULL;
}

static xmlNode*
lsw_builder_get_ref(    xmlNode*    node,
                        int         idx     )
{
    return lsw_builder_get_ref_priv(node, &idx);
}

static void
lsw_builder_replace_ref(    xmlNode*    ref_node,
                            xmlDoc*     ref_doc     )
{
    // Get parent and remove ref from tree
    xmlNode* ref_parent = ref_node->parent;
    xmlUnlinkNode(ref_node);
    
    // Foreach child within <interface/>
    xmlNode* child = xmlDocGetRootElement(ref_doc)->children;
    while (child != NULL)
    {
        xmlNode* next = child->next;
        
        // Unlink from ref doc
        xmlUnlinkNode(child);
        
        // Add children from ref-node
        if (child->type == XML_ELEMENT_NODE)
        {
            xmlNode* ph = ref_node->children;
            while (ph != NULL)
            {
                xmlNode* ph_copy = xmlCopyNode(ph, TRUE);
                xmlAddChild(child, ph_copy);
                ph = ph->next;
            }
        }
        
        // Add to base doc
        xmlNode* res = xmlAddChild(ref_parent, child);
        
        child = next;
    }
    
    // Free ref node
    xmlFreeNode(ref_node);
    ref_node = NULL;
}

static xmlDoc*
lsw_builder_process(    LswBuilder*     self,
                        const char*     buffer,
                        const char*     path    )
{
    //xmlDoc* doc = xmlParseMemory(buffer, (int)strlen(buffer));
    xmlDoc* doc = xmlReadMemory(
        buffer,
        (int)strlen(buffer),
        "",
        NULL,
        XML_PARSE_RECOVER
    );
    if (!doc)
        g_error("Failed to parse");
    
    // Check if root is interface
    xmlNode* root_node = xmlDocGetRootElement(doc);
    if (!root_node)
        g_error("Failed to parse: empty document");
    else if (strcmp((char*)root_node->name, "interface") != 0)
        g_error("Failed to parse: not a ui definition");
    
    // Rename all ID's
    lsw_builder_process_rename_id(root_node, path);
    
    // Foreach ui ref
    for (int i = 0; ; i++)
    {
        // Get node
        xmlNode* ref_node = lsw_builder_get_ref(root_node, 0);
        if (!ref_node)
            break;
        
        // Get ID & source
        const char* id = (const char*)xmlGetProp(ref_node, (xmlChar*)"id");
        const char* source = (const char*)xmlGetProp(ref_node, (xmlChar*)"source");
        if (!id || !source)
            g_error("LswUI node: id or source missing");
        
        // Load source if it is a file
        if (!lsw_builder_get_source(self, source))
        {
            char* data = lsw_builder_read_file(self, source);
            lsw_builder_set_source_priv(self, source, data, FALSE);
        }
        
        // Prepare ref path
        static char ref_path[MAX_ID_LENGTH];
        // "path" always ends with /
        // "id" never contains a /
        snprintf(ref_path, MAX_ID_LENGTH, "%s%s/", path, id);
        
        // Get doc from reference
        const char* ref_buffer = lsw_builder_get_source(self, source);
        xmlDoc* ref_doc = lsw_builder_process(self, ref_buffer, ref_path);
        
        // Replace ref node with doc content
        lsw_builder_replace_ref(ref_node, ref_doc);
        
        // Free ref doc
        xmlFreeDoc(ref_doc);
    }
    
    return doc;
}