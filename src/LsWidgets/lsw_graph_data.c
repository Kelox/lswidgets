#include <LsWidgets/lsw_graph_data.h>
#include <math.h>
#include <LsWidgets/lsw_utils.h>


// MACROS //
#define GENERIC_GET_RANGE_1D(DATATYPE) \
    for (int idx = start_x; idx <= end_x; idx += resolution) \
    { \
        out_x[cnt] = (float)idx; \
        out_y[cnt] = (float)((DATATYPE*)data)[idx]; \
        cnt++; \
        if (out_length <= cnt) break; \
    }

#define GENERIC_BIN_SEARCH_VALUE(DATATYPE) { \
    DATATYPE* data = (DATATYPE*)ph_data; \
    int left = 0; \
    int right = data_length - 1; \
    if (value < (float)data[left] || value > (float)data[right]) \
        return -1; \
     \
    while ((right - left) > 1) \
    { \
        int center = (right - left) / 2 + left; \
        if ((float)data[left] <= value && (float)data[center] >= value) \
            right = center; \
        else \
            left = center; \
    } \
     \
    if (fabsf(value - (float)data[left]) < fabsf(value - (float)data[right])) \
        return left; \
    return right; } \


// PROTOTYPES //
static void
dispose(GObject* self_object);

static void
finalize(GObject* self_object);

static void
lsw_graph_data_lock_when_returned(LswGraphData* self);

// TODO: after every set_data and return
static void
lsw_graph_data_update_bounds(LswGraphData* self);

static inline int
get_from_range_1d(  LswGraphData*   self,
                    void*           data,
                    float           r_resolution,
                    float           r_start_x,
                    float           r_end_x,
                    float*          out_x,
                    float*          out_y,
                    int             out_length      );

static inline int
get_from_range_2d(  LswGraphData*   self,
                    void*           data,
                    float           r_resolution,
                    float           r_start_x,
                    float           r_end_x,
                    float*          out_x,
                    float*          out_y,
                    int             out_length      );


// TYPE DEFINITION //
struct _LswGraphData
{
    GObject parent;
    
    gboolean disposed;
    void* data;
    int data_length;
    enum LswGraphDataType data_type;
    gboolean is_2d;
    
    GMutex mutex, set_mutex;
    gboolean borrowed;
};
G_DEFINE_FINAL_TYPE(LswGraphData, lsw_graph_data, G_TYPE_OBJECT);


// CONSTRUCTOR / DECONSTRUCTOR //
static void
lsw_graph_data_class_init(LswGraphDataClass* klass)
{
    // Overwrite virtual functions
    G_OBJECT_CLASS(klass)->dispose = &dispose;
    G_OBJECT_CLASS(klass)->finalize = &finalize;
}

static void
lsw_graph_data_init(LswGraphData* self)
{
    self->disposed = FALSE;
    self->data = NULL;
    self->data_length = 0;
    self->data_type = LSW_GRAPH_DATA_TYPE_NONE;
    g_mutex_init(&self->mutex);
    g_mutex_init(&self->set_mutex);
}

static void
dispose(GObject* self_object)
{
    LswGraphData* self = LSW_GRAPH_DATA(self_object);
    
    if (!self->disposed)
    {
        
        self->disposed = TRUE;
    }
    
    // Chain
    G_OBJECT_CLASS(lsw_graph_data_parent_class)->dispose(self_object);
}

static void
finalize(GObject* self_object)
{
    LswGraphData* self = LSW_GRAPH_DATA(self_object);
    
    lsw_graph_data_set_data(self, NULL, 0, LSW_GRAPH_DATA_TYPE_NONE, FALSE);
    g_mutex_clear(&self->mutex);
    g_mutex_clear(&self->set_mutex);
    
    // Chain
    G_OBJECT_CLASS(lsw_graph_data_parent_class)->finalize(self_object);
}


// PUBLIC FUNCTIONS //
LswGraphData*
lsw_graph_data_new()
{
    LswGraphData* self = LSW_GRAPH_DATA(g_object_new(
        LSW_TYPE_GRAPH_DATA,
        NULL
    ));
    return self;
}

void
lsw_graph_data_set_data(    LswGraphData*           self,
                            void*                   data,
                            int                     data_length,
                            enum LswGraphDataType   data_type,
                            gboolean                is_2d       )
{
    // Lock so that the data definitely won't change while we're at it
    g_mutex_lock(&self->set_mutex);
    
    // Free previous data
    void* prev_data;
    int prev_data_length;
    lsw_graph_data_borrow(self, &prev_data, &prev_data_length);
    if (prev_data)
    {
        lsw_graph_data_take(self, prev_data);
        free(prev_data);
    }
    
    // Set new data
    g_mutex_lock(&self->mutex);
    self->data = data;
    self->data_length = data_length;
    self->data_type = data_type;
    self->is_2d = is_2d;
    if (self->borrowed) // shouldn't be possible, but just in case
    {
        g_error("Set data while it was borrowed?!");
    }
    else if (self->data &&
        (self->data_length <= 0 || self->data_type == LSW_GRAPH_DATA_TYPE_NONE))
    {
        g_error("Got data with a length of <0 or type NONE");
    }
    g_mutex_unlock(&self->mutex);
    
    // Unlock setter
    g_mutex_unlock(&self->set_mutex);
}

void
lsw_graph_data_borrow(    LswGraphData*   self,
                          void**          data,
                          int*            data_length )
{
    lsw_graph_data_lock_when_returned(self);
    if (!self->data)
    {
        *data = NULL;
        data_length = 0;
        g_mutex_unlock(&self->mutex);
        return;
    }
    *data = self->data;
    *data_length = self->data_length;
    self->borrowed = TRUE;
    g_mutex_unlock(&self->mutex);
}

void
lsw_graph_data_return(  LswGraphData*   self,
                        void**          data    )
{
    g_mutex_lock(&self->mutex);
    if (!self->borrowed)
        g_error("Returned data that was never borrowed");
    else if (self->data != *data)
        g_error("Returned data that did not belong to this LswGraphData");
    *data = NULL;
    self->borrowed = FALSE;
    g_mutex_unlock(&self->mutex);
}

void
lsw_graph_data_take(    LswGraphData*   self,
                        void*           data    )
{
    g_mutex_lock(&self->mutex);
    if (!self->borrowed)
        g_error("Took data that was never borrowed");
    else if (self->data != data)
        g_error("Took data that did not belong to this LswGraphData");
    self->borrowed = FALSE;
    self->data = NULL; // ownership moved to caller of this function
    self->data_length = 0;
    self->data_type = LSW_GRAPH_DATA_TYPE_NONE;
    g_mutex_unlock(&self->mutex);
}

int
lsw_graph_data_get_from_range(  LswGraphData*   self,
                                void*           data,
                                float           r_resolution,
                                float           r_start_x,
                                float           r_end_x,
                                float*          out_x,
                                float*          out_y,
                                int             out_length      )
{
    // Check conditions
    if (out_length == 0)
        return 0;
    else if (!out_x || !out_y)
        g_error("lsw_graph_data_get_from_range: point array is NULL");
    else if (r_resolution <= 0)
        g_error("lsw_graph_data_get_from_range: invalid resolution");
    
    // Debug borrow check
#ifdef _DEBUG
    g_mutex_lock(&self->mutex);
    if (!self->borrowed)
        g_error("lsw_graph_data_get_from_range: data was never borrowed");
    else if (self->data != data)
        g_error("lsw_graph_data_get_from_range: data does not belong to this LswGraphData");
    else if (self->data_type == LSW_GRAPH_DATA_TYPE_NONE)
        g_error("lsw_graph_data_get_from_range: data type is none");
    else if (!self->data || self->data_length <= 0)
        g_error("lsw_graph_data_get_from_range: data is NULL or has a length of <0");
    g_mutex_unlock(&self->mutex);
#endif
    
    // Get data
    if (self->is_2d)
    {
        return get_from_range_2d(
            self,
            data,
            r_resolution,
            r_start_x,
            r_end_x,
            out_x,
            out_y,
            out_length
        );
    }
    else
    {
        return get_from_range_1d(
            self,
            data,
            r_resolution,
            r_start_x,
            r_end_x,
            out_x,
            out_y,
            out_length
        );
    }
}


// PRIVATE FUNCTIONS //
static void
lsw_graph_data_lock_when_returned(LswGraphData* self)
{
    while (TRUE)
    {
        g_mutex_lock(&self->mutex);
        if (!self->borrowed)
            break;
        g_mutex_unlock(&self->mutex);
        g_usleep(10);
    }
}

static inline int
get_from_range_1d(  LswGraphData*   self,
                    void*           data,
                    float           r_resolution,
                    float           r_start_x,
                    float           r_end_x,
                    float*          out_x,
                    float*          out_y,
                    int             out_length      )
{
    // Get parameters as integers & clamp range
    int resolution = (int)ceilf(r_resolution);
    int start_x = (int)floorf(r_start_x);
    start_x = LSW_CLAMP(start_x, 0, self->data_length - 1);
    int end_x = (int)ceilf(r_end_x);
    end_x = LSW_CLAMP(end_x, 0, self->data_length - 1);
    if (start_x > end_x)
        return 0;
    
    // Write values
    int cnt = 0;
    switch (self->data_type)
    {
        case LSW_GRAPH_DATA_TYPE_INT:
            GENERIC_GET_RANGE_1D(int);
            break;
        case LSW_GRAPH_DATA_TYPE_BYTE:
            GENERIC_GET_RANGE_1D(unsigned char);
            break;
        case LSW_GRAPH_DATA_TYPE_FLOAT:
            GENERIC_GET_RANGE_1D(float);
            break;
        case LSW_GRAPH_DATA_TYPE_DOUBLE:
            GENERIC_GET_RANGE_1D(double);
            break;
        default:
            g_error("Invalid data type (not implemented yet?)");
            break;
    }
    
    return cnt;
}

static inline int
bin_search_value(   void*                   ph_data,
                    int                     data_length,
                    enum LswGraphDataType   data_type,
                    float                   value       )
{
    if (data_length <= 0)
        return -1;
    
    switch (data_type)
    {
        case LSW_GRAPH_DATA_TYPE_INT:
            GENERIC_BIN_SEARCH_VALUE(int);
        case LSW_GRAPH_DATA_TYPE_BYTE:
            GENERIC_BIN_SEARCH_VALUE(unsigned char);
        case LSW_GRAPH_DATA_TYPE_FLOAT:
            GENERIC_BIN_SEARCH_VALUE(float);
        case LSW_GRAPH_DATA_TYPE_DOUBLE:
            GENERIC_BIN_SEARCH_VALUE(double);
        default:
            g_error("Invalid data type (not implemented yet?)");
    }
}

static inline int
get_from_range_2d(  LswGraphData*   self,
                    void*           data,
                    float           r_resolution,
                    float           r_start_x,
                    float           r_end_x,
                    float*          out_x,
                    float*          out_y,
                    int             out_length      )
{
    
    // FIXME: not implemented yet
    g_error("Not implemented yet");
    
    // Check conditions
    if (r_start_x > r_end_x)
        return 0;
    
    // Get search range
    int left = bin_search_value(data, self->data_length, self->data_type, r_start_x);
    int right = bin_search_value(data, self->data_length, self->data_type, r_end_x);
    if (left == -1 && right == -1)
        return 0;
    else if (left == -1)
        left = 0;
    else if (right == -1)
        right = self->data_length - 1;
    
    int resolution = (int)ceilf((r_end_x - r_start_x) / r_resolution);
    if (resolution <= 0)
        resolution = 1;
    
    // Write values
    int cnt = 0;
    /*for (int idx = left; idx <= right; idx += resolution)
    {
        int idx = bin_search_value(data, self->data_length, self->data_type, value);
        out_x[cnt] = (float)((int*)data)[idx * 2];
        out_y[cnt] = (float)((int*)data)[idx * 2 + 1];
        cnt++;
        if (out_length <= cnt)break;
    }*/
    
    return cnt;
}