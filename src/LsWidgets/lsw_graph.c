#include <LsWidgets/lsw_graph.h>
#include <LsWidgets/lsw_utils.h>


// MACROS //
#define AXIS_LINE_HEIGHT 16
#define AXIS_TITLE_HEIGHT 32
#define AXIS_LABEL_HEIGHT 16
#define AXIS_SPACING 4
#define AXIS_HEIGHT (AXIS_TITLE_HEIGHT + AXIS_SPACING + \
    AXIS_LINE_HEIGHT + AXIS_SPACING + \
    AXIS_LABEL_HEIGHT)

#define AXIS_PIXEL_PER_STEP 128

#define CONTENT_SPACING 8

#define CAIRO_THEMED_RGBA(CAIRO, R, G, B, A) { \
    if (lsw_utils_get_dark_mode()) \
        cairo_set_source_rgba(CAIRO, 1.0 - (R), 1.0 - (G), 1.0 - (B), (A)); \
    else \
        cairo_set_source_rgba(CAIRO, (R), (G), (B), (A)); } \

#define CAIRO_THEMED_RGB(CAIRO, R, G, B) CAIRO_THEMED_RGBA(CAIRO, R, G, B, 1.0)


// PROTOTYPES //
static void
dispose(GObject* self_object);

static void
finalize(GObject* self_object);

static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       );

static void
get_property(   GObject*    self_object,
                guint       property_id,
                GValue*     value,
                GParamSpec* pspec       );

static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget);

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline    );

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap        );

static gboolean
on_tick(    GtkWidget*      self_widget,
            GdkFrameClock*  frame_clock,
            gpointer        user_data       );

static void
lsw_graph_draw_axis(    LswGraph*       self,
                        LswGraphAxis*   axis,
                        cairo_t*        cairo,
                        float           length,
                        float           height  );


// ENUMS //
enum LswGraphProperties
{
    PROP_AXIS_LEFT = 1,
    PROP_AXIS_BOTTOM,
    PROP_AXIS_RIGHT,
    PROP_AXIS_TOP,
    
    N_PROPERTIES
};
static GParamSpec* props[N_PROPERTIES] = { NULL };


// STRUCTS //
typedef struct
{
    gboolean axis_left;
    gboolean axis_bottom;
    LswGraphData* data;
    GdkRGBA color;
} data_binding_t;


// TYPE DEFINITION //
struct _LswGraph
{
    GtkWidget parent;
    
    gboolean disposed;
    LswGraphAxis* axis_left;
    LswGraphAxis* axis_bottom;
    LswGraphAxis* axis_right;
    LswGraphAxis* axis_top;
    
    GSList* data_list;
};
G_DEFINE_FINAL_TYPE(LswGraph, lsw_graph, GTK_TYPE_WIDGET);


// CONSTRUCTOR / DECONSTRUCTOR //
static void
lsw_graph_class_init(LswGraphClass* klass)
{
    // Virtual functions
    G_OBJECT_CLASS(klass)->dispose = &dispose;
    G_OBJECT_CLASS(klass)->finalize = &finalize;
    G_OBJECT_CLASS(klass)->set_property = &set_property;
    G_OBJECT_CLASS(klass)->get_property = &get_property;
    GTK_WIDGET_CLASS(klass)->get_request_mode = &get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = &measure;
    GTK_WIDGET_CLASS(klass)->snapshot = &snapshot;
    
    // Install properties
    props[PROP_AXIS_LEFT] = g_param_spec_object(
        "axis-left", "AxisLeft", "Left Axis", LSW_TYPE_GRAPH_AXIS,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_AXIS_BOTTOM] = g_param_spec_object(
        "axis-bottom", "AxisBottom", "Bottom Axis", LSW_TYPE_GRAPH_AXIS,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_AXIS_RIGHT] = g_param_spec_object(
        "axis-right", "AxisRight", "Right Axis", LSW_TYPE_GRAPH_AXIS,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    props[PROP_AXIS_TOP] = g_param_spec_object(
        "axis-top", "AxisTop", "Top Axis", LSW_TYPE_GRAPH_AXIS,
        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
    );
    
    g_object_class_install_properties(
        G_OBJECT_CLASS(klass),
        N_PROPERTIES,
        props
    );
}

static void
lsw_graph_init(LswGraph* self)
{
    // Init fields
    self->disposed = FALSE;
    self->axis_left = NULL;
    self->axis_bottom = NULL;
    self->axis_right = NULL;
    self->axis_top = NULL;
    self->data_list = NULL;
    
    // Signals
    gtk_widget_add_tick_callback(
        GTK_WIDGET(self),
        &on_tick,
        NULL,
        NULL
    );
}

static void
dispose(GObject* self_object)
{
    LswGraph* self = LSW_GRAPH(self_object);
    
    if (!self->disposed)
    {
        if (self->axis_left)
        {
            g_object_unref(self->axis_left);
            self->axis_left = NULL;
        }
        if (self->axis_bottom)
        {
            g_object_unref(self->axis_bottom);
            self->axis_bottom = NULL;
        }
        if (self->axis_right)
        {
            g_object_unref(self->axis_right);
            self->axis_right = NULL;
        }
        if (self->axis_top)
        {
            g_object_unref(self->axis_top);
            self->axis_top = NULL;
        }
        
        self->disposed = TRUE;
    }
    
    // Chain up
    G_OBJECT_CLASS(lsw_graph_parent_class)->dispose(self_object);
}

static void
finalize(GObject* self_object)
{
    LswGraph* self = LSW_GRAPH(self_object);
    
    while (g_slist_length(self->data_list) > 0)
    {
        data_binding_t* db = g_slist_nth_data(self->data_list, 0);
        lsw_graph_remove_data(self, db->data);
    }
    
    // Chain up
    G_OBJECT_CLASS(lsw_graph_parent_class)->finalize(self_object);
}


// Widget virtual functions //
static GtkSizeRequestMode
get_request_mode(GtkWidget* self_widget)
{
    return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline    )
{
    *minimum = AXIS_HEIGHT * 2;
    *natural = AXIS_HEIGHT * 2 * 3;
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap        )
{
    LswGraph* self = LSW_GRAPH(self_widget);
    
    // Get bounds
    graphene_rect_t rect;
    (void)gtk_widget_compute_bounds(
        self_widget, self_widget,
        &rect
    );
    
    // Determine content area
    float cont_left = self->axis_left ? AXIS_HEIGHT : 0;
    cont_left += CONTENT_SPACING;
    float cont_right = self->axis_right ? rect.size.width - AXIS_HEIGHT : rect.size.width;
    cont_right -= CONTENT_SPACING;
    float cont_top = self->axis_top ? AXIS_HEIGHT : 0;
    cont_top += CONTENT_SPACING;
    float cont_bottom = self->axis_bottom ? rect.size.height - AXIS_HEIGHT : rect.size.height;
    cont_bottom -= CONTENT_SPACING;
    
    if (cont_right < cont_left || cont_bottom < cont_top)
        return;
    
    // Get cairo
    cairo_t* cairo = gtk_snapshot_append_cairo(
        snap,
        &GRAPHENE_RECT_INIT(
            0, 0,
            rect.size.width, rect.size.height
        )
    );
    
    // Highlight content area
    cairo_save(cairo);
    cairo_set_line_width(cairo, 0.5);
    cairo_rectangle(
        cairo,
        cont_left, cont_top,
        cont_right - cont_left,
        cont_bottom - cont_top
    );
    CAIRO_THEMED_RGB(cairo, 0, 0, 0);
    cairo_stroke(cairo);
    cairo_restore(cairo);
    
    // Draw message if no axes
    cairo_save(cairo);
    if (!((self->axis_left || self->axis_right) && (self->axis_top || self->axis_bottom)))
    {
        // Configure font
        cairo_select_font_face(
            cairo, "monospace", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL
        );
        cairo_set_font_size(cairo, 20.0);
        static const char* const no_axes_msg = "(Graph Axes Missing)";
        cairo_text_extents_t extents;
        cairo_text_extents(cairo, no_axes_msg, &extents);
        
        cairo_move_to(
            cairo,
            (rect.size.width - extents.width) / 2.0,
            (rect.size.height + extents.height) / 2.0
        );
        CAIRO_THEMED_RGB(cairo, 0, 0, 0);
        cairo_show_text(cairo, no_axes_msg);
        
        cairo_restore(cairo);
        cairo_destroy(cairo);
        return;
    }
    cairo_restore(cairo);
    
    
    // Draw axes
    if (self->axis_top)
    {
        cairo_save(cairo);
        cairo_translate(cairo, cont_left, 0);
        lsw_graph_draw_axis(
            self, self->axis_top, cairo, cont_right - cont_left, AXIS_HEIGHT
        );
        cairo_restore(cairo);
    }
    
    // Clean up
    cairo_destroy(cairo);
    
    /*GdkRGBA color;
    gdk_rgba_parse(&color, "#FF0000");
    
    gtk_snapshot_append_color(
        snap,
        &color,
        &GRAPHENE_RECT_INIT(
            0, 0,
            cont_right - cont_left, cont_bottom - cont_top
        )
    );*/
    
    /*static int start = 0;
    start = (start + 1) % 10000;
    
    GSList* node = self->data_list;
    while (node != NULL)
    {
        data_binding_t* db = (data_binding_t*)node->data;
        
        void* data = NULL;
        int data_length = 0;
        
        lsw_graph_data_borrow(db->data, &data, &data_length);
        static float idkx[256];
        static float idky[256];
        int cnt = lsw_graph_data_get_from_range(
            db->data, data,
            1.0f, (float)start, (float)start + 256.0f,
            idkx, idky, 256
        );
        lsw_graph_data_return(db->data, &data);
        
        for (int i = 0; i < cnt; i++)
        {
            gtk_snapshot_append_color(
                snap,
                &color,
                &GRAPHENE_RECT_INIT(
                    (((float)i / (float)cnt) * rect.size.width),
                    (rect.size.height / 2.0f) * (0.7f * idky[i] + 1.0f),
                    5, 5
                )
            );
        }
        
        node = node->next;
    }*/
}

static gboolean
on_tick(    GtkWidget*      self_widget,
            GdkFrameClock*  frame_clock,
            gpointer        user_data       )
{
    LswGraph* self = LSW_GRAPH(self_widget);
    if (self->disposed)
        return G_SOURCE_REMOVE;
    
    // TODO: this is only temporary!
    gtk_widget_queue_draw(self_widget);
    
    return G_SOURCE_CONTINUE;
}



// GObject virtual functions //
static void
set_property(   GObject*        self_object,
                guint           property_id,
                const GValue*   value,
                GParamSpec*     pspec       )
{
    LswGraph* self = LSW_GRAPH(self_object);
    
    switch (property_id)
    {
        case PROP_AXIS_LEFT:
        case PROP_AXIS_BOTTOM:
        case PROP_AXIS_RIGHT:
        case PROP_AXIS_TOP:
        {
            // Get pointer to the axis
            LswGraphAxis** self_axis = NULL;
            switch (property_id)
            {
                case PROP_AXIS_LEFT: self_axis = &self->axis_left; break;
                case PROP_AXIS_BOTTOM: self_axis = &self->axis_bottom; break;
                case PROP_AXIS_RIGHT: self_axis = &self->axis_right; break;
                case PROP_AXIS_TOP: self_axis = &self->axis_top; break;
                default: g_error("Unknown axis type"); // impossible
            }
            
            // Unref old axis
            if (*self_axis)
            {
                g_object_unref(*self_axis);
                *self_axis = NULL;
            }
            GObject* new_axis = g_value_get_object(value);
            if (new_axis)
            {
                g_object_ref(new_axis);
                *self_axis = LSW_GRAPH_AXIS(new_axis);
            }
            break;
        }
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}

static void
get_property(   GObject*    self_object,
                guint       property_id,
                GValue*     value,
                GParamSpec* pspec       )
{
    LswGraph* self = LSW_GRAPH(self_object);
    
    switch (property_id)
    {
        case PROP_AXIS_LEFT: g_value_set_object(value, self->axis_left); break;
        case PROP_AXIS_BOTTOM: g_value_set_object(value, self->axis_bottom); break;
        case PROP_AXIS_RIGHT: g_value_set_object(value, self->axis_right); break;
        case PROP_AXIS_TOP: g_value_set_object(value, self->axis_top); break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(self_object, property_id, pspec);
            break;
    }
}



// PUBLIC FUNCTIONS //
void
lsw_graph_add_data( LswGraph*       self,
                    LswGraphData*   data,
                    gboolean        axis_left,
                    gboolean        axis_bottom,
                    GdkRGBA         color       )
{
    data_binding_t* db = (data_binding_t*)malloc(sizeof(data_binding_t));
    if (!db)
        g_error("Malloc failed");
    
    // Init struct
    db->axis_left = axis_left;
    db->axis_bottom = axis_bottom;
    db->data = data;
    db->color = color;
    
    // Ref objects
    g_object_ref(db->data);
    
    // Add struct to list
    self->data_list = g_slist_append(self->data_list, db);
}

void
lsw_graph_remove_data(  LswGraph*       self,
                        LswGraphData*   data    )
{
    // Get list node
    GSList* node = self->data_list;
    while (node && ((data_binding_t*)node->data)->data != data)
        node = node->next;
    if (!node)
        g_error("Data is not contained in this graph");
    
    // Get binding & remove link from list
    data_binding_t* db = (data_binding_t*)node->data;
    self->data_list = g_slist_remove_link(self->data_list, node);
    g_slist_free(node);
    node = NULL;
    
    // Unref object
    g_object_unref(db->data);
    db->data = NULL;
    
    // Free binding
    free(db);
}


// PRIVATE FUNCTIONS //
static void
lsw_graph_draw_axis(    LswGraph*       self,
                        LswGraphAxis*   axis,
                        cairo_t*        cairo,
                        float           length,
                        float           height  )
{
    float min, max;
    lsw_graph_axis_get_display_min_max(axis, &min, &max);
    lsw_graph_axis_set_display_min(axis, min - 0.1f);
    lsw_graph_axis_set_display_max(axis, max + 0.1f);
    if ((max - min) <= 0)
        return;
    
    // Step count request
    int req_steps = (int)floorf(length / AXIS_PIXEL_PER_STEP);
    
    // Find step width that fulfills the request the most
    double step_width = 1.0;
    int step_count = (int)ceil((double)(max - min) / step_width);
    int dir = (step_count > req_steps ? 1 : -1);
    
    static double ph[] = { 1, 2, 5 };
    int expo = 0;
    for (int i = (dir < 0 ? 2 : 0); ; i++)
    {
        double prev_step_width = step_width;
        int prev_step_count = step_count;
        
        // Update step width
        if (dir > 0)
            step_width = 1.0 * ph[i % 3] * pow(10.0, expo);
        else
            step_width = 1.0 * ph[2 - (i % 3)] * pow(10.0, expo);
        step_count = (int)ceil((double)(max - min) / step_width);
        
        // Done?
        if (abs(prev_step_count - req_steps) < abs(step_count - req_steps))
        {
            step_count = prev_step_count;
            step_width = prev_step_width;
            break;
        }
        
        // Next
        if ((i % 3) == 2)
            expo += dir;
    }
    
    // Get start pos that is divisible by the step width
    step_count++;
    float start_x = (float)(floor((double)min / step_width) * step_width);
    
    
    // Draw
    cairo_translate(cairo, 0, AXIS_TITLE_HEIGHT + AXIS_SPACING);
    cairo_save(cairo);
    
    // Select font
    cairo_select_font_face(
        cairo, "monospace", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL
    );
    cairo_set_font_size(cairo, AXIS_LABEL_HEIGHT);
    cairo_font_extents_t font_extents;
    cairo_font_extents(cairo, &font_extents);
    
    // Draw main line
    cairo_set_line_width(cairo, 1.0);
    cairo_move_to(cairo, 0, AXIS_LINE_HEIGHT * 0.5);
    cairo_line_to(cairo, length, AXIS_LINE_HEIGHT * 0.5);
    
    for (int i = 0; i <= step_count; i++)
    {
        float pos = (float)(start_x + i * step_width);
        float pix = (length / (max - min)) * (float)(pos - min);
        if (fabsf(pos) < 0.000001f)
            pos = 0;
        //if (pix > length)
        //    break;
        
        // Draw line
        cairo_move_to(cairo, pix, 0);
        cairo_line_to(cairo, pix, AXIS_LINE_HEIGHT);
        
        // Draw text
        static char txt[64];
        snprintf(txt, 64, "%.6g", pos);
        cairo_text_extents_t extents;
        cairo_text_extents(cairo, txt, &extents);
        
        cairo_rel_move_to(cairo, -extents.width / 2.0f, AXIS_SPACING + extents.height);
        cairo_show_text(cairo, txt);
    }
    
    CAIRO_THEMED_RGB(cairo, 0, 0, 0);
    cairo_stroke(cairo);
    
    cairo_restore(cairo);
    
}