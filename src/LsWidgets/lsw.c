#include <LsWidgets/lsw.h>
#include <LsWidgets/lsw_utils.h>


// PUBLIC FUNCTIONS //
void
ls_widgets_init()
{
    // Ensure types
    g_type_ensure(LSW_TYPE_BUILDER);
    g_type_ensure(LSW_TYPE_LED);
    
    // Determine system theme
#ifdef __linux__
    FILE* cmdfile = popen("G_MESSAGES_DEBUG=none gsettings get org.gnome.desktop.interface color-scheme", "r");
    if (cmdfile)
    {
        char buf[128];
        if (fgets(buf, 128, cmdfile) && strstr(buf, "dark"))
            lsw_utils_set_dark_mode(TRUE);
        else
            lsw_utils_set_dark_mode(FALSE);
        
        pclose(cmdfile);
    }
#elif defined(_WIN32)
    g_warning("Windows theme color detection not implemented yet");
    lsw_utils_set_dark_mode(FALSE);
#else
    lsw_utils_set_dark_mode(FALSE);
#endif
}

void
ls_widgets_cleanup()
{

}